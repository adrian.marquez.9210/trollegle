package anon.trollegle;

public interface Callback<T extends UserConnection> {
    void callback(T user, String action, String parameter);
    default void callback(T user, String action, int parameter) { };
}
